#include <stdio.h>
#include <stdlib.h>
#include <wiringPi.h>
#include <lcd.h>
#include <time.h>
//#include <iostream>
//using namespace std;

#define ButtonPin 29

const unsigned char Buf[] = "---SUNFOUNDER---";
const unsigned char myBuf[] = "  sunfounder.com";



int main(void)
{
	
	int fd;
	int i;
	if (wiringPiSetup() == -1){
		exit(1);
	}

	pinMode(ButtonPin, INPUT);
	pullUpDnControl(ButtonPin, PUD_UP);

	fd = lcdInit(2,16,4, 2,3, 6,5,4,1,0,0,0,0); //see /usr/local/include/lcd.h
	//printf("%d", fd);
	if (fd == -1){
		printf("lcdInit 1 failed\n") ;
		return 1;
	}
	/*
	lcdPosition(fd, 0 ,0);
	lcdPuts(fd, "boot successfull");
	lcdPosition(fd, 0 ,1);
	lcdPuts(fd, "starting clock");

	sleep(4);	

	int timeout = 10;
	lcdClear(fd);
	for (timeout; timeout > 0; timeout --) {
		
		lcdPosition(fd, 0 ,0);
		lcdPuts(fd, "Waiting");
		
		char wtime[16];
		sprintf(wtime, "%d Seconds", timeout);
		lcdPosition(fd, 0 ,1);
		lcdPuts(fd, wtime);
		sleep(1);
		lcdClear(fd);	
	
	}
	*/
	
	while (1) {
	
	time_t current_time;
	    char* c_time_string;
	
	    /* Obtain current time. */
	    current_time = time(NULL);
	
	    if (current_time == ((time_t)-1))
	    {
	        (void) fprintf(stderr, "Failure to obtain the current time.\n");
	        exit(EXIT_FAILURE);
	    }
	
	    /* Convert to local time format. */
	    c_time_string = ctime(&current_time);


	    if (c_time_string == NULL)
	    {
	        (void) fprintf(stderr, "Failure to convert the current time.\n");
	        exit(EXIT_FAILURE);
	    }
	
	    /* Print to stdout. ctime() has already added a terminating newline character. */
	    // printf("Current time is %s", c_time_string);
    		//exit(EXIT_SUCCESS);
		
		char date[16];
		char time[16];
		int k;
		int dateIndex = 0;
		int timeIndex = 0;
		for (k = 0; k < 24; k++) {
			if (k < 11 || k > 19) {
				date[dateIndex] = c_time_string[k];
				dateIndex++;
			} else if (k > 10 && k < 19) {
				time[timeIndex] = c_time_string[k];
				timeIndex++;
			}
		}
		date[dateIndex] = '\0';
		time[timeIndex] = '\0';

		//printf("it is %s\n",time);
		//printf("and today is %s\n",date);
		
		//lcdClear(fd);
		lcdPosition(fd, 0 ,0);
		lcdPuts(fd, date);

		lcdPosition(fd, 0 ,1);
		lcdPuts(fd, time);
		sleep(0,5);

	
		if(digitalRead(ButtonPin) == 0) {
			lcdClear(fd);
			lcdPosition(fd, 0 ,0);
			lcdPuts(fd, "Computer");
			lcdPosition(fd, 0 ,1);
			lcdPuts(fd, "sagt nein!");
			sleep(3);
			lcdClear(fd);
			
		}
	}

 
  	

	return 0;
}
