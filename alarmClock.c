#include <stdio.h>
#include <stdlib.h>
#include <wiringPi.h>
#include <lcd.h>
#include <time.h>
#include <string.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>
#include <arpa/inet.h>

#define SelectPin 29
#define CyclePin 28
#define SetPin 25
#define StartPin 24

#define BeepPin 27

//#define Path "\\home\\pi\\Sunfounder_SuperKit_C_code_for_RaspberryPi\\own projects\\LCD_Display\\save.txt"
#define Path "../save.txt"
static int alarmOn;
static int startPcOn;
static struct tm *alarmTime;

#define BeepOff LOW
#define BeepOn HIGH

void initButtons() {
	pinMode(BeepPin, OUTPUT);   //set GPIO0 output
	digitalWrite(BeepPin, BeepOff); //beep off

	pinMode(SelectPin, INPUT);
	pullUpDnControl(SelectPin, PUD_UP);

	pinMode(CyclePin, INPUT);
	pullUpDnControl(CyclePin, PUD_UP);

	pinMode(SetPin, INPUT);
	pullUpDnControl(SetPin, PUD_UP);

	pinMode(StartPin, INPUT);
	pullUpDnControl(StartPin, PUD_UP);
}

void saySomething(int fd, char* say) {
	lcdClear(fd);
	lcdPosition(fd, 0 ,0);
	lcdPuts(fd, say);
	sleep(2);
	lcdClear(fd);
}

int isSameMinute(const struct tm *timeToCompare) {
	time_t current_time = time(NULL);
	struct tm *currentTime = localtime(&current_time);
	if (timeToCompare->tm_hour == currentTime->tm_hour 
		&& timeToCompare->tm_min == currentTime->tm_min) {
		return 1;
	} else {
		return 0;
	}
}

void printCurrentTimeToScreen(int fd) {
		time_t current_time;
	    	char* c_time_string;
	
	    	/* Obtain current time. */
	    	current_time = time(NULL);
	
	    	if (current_time == ((time_t)-1))
	    	{
	        	(void) fprintf(stderr, "Failure to obtain the current time.\n");
	        	exit(EXIT_FAILURE);
	    	}
	
	    	/* Convert to local time format. */
	    	c_time_string = ctime(&current_time);


	    	if (c_time_string == NULL)
	    	{
	        	(void) fprintf(stderr, "Failure to convert the current time.\n");
	        	exit(EXIT_FAILURE);
	    	}
	
	    	/* Print to stdout. ctime() has already added a terminating newline character. */
	    	// printf("Current time is %s", c_time_string);
    		//exit(EXIT_SUCCESS);
		
		char date[16];
		char time[16];
		int k;
		int dateIndex = 0;
		int timeIndex = 0;
		for (k = 0; k < 24; k++) {
			if (k < 11 || k > 19) {
				date[dateIndex] = c_time_string[k];
				dateIndex++;
			} else if (k > 10 && k < 19) {
				time[timeIndex] = c_time_string[k];
				timeIndex++;
			}
		}
		date[dateIndex] = '\0';
		if (alarmOn) {
			time[timeIndex++] = ' ';
			time[timeIndex++] = 'a';
			time[timeIndex++] = 'r';
			time[timeIndex++] = 'm';
			time[timeIndex++] = 'e';
			time[timeIndex++] = 'd';
		} else {
			time[timeIndex++] = ' ';
			time[timeIndex++] = ' ';
			time[timeIndex++] = ' ';
			time[timeIndex++] = ' ';
			time[timeIndex++] = ' ';
			time[timeIndex++] = ' ';
		}
		time[timeIndex] = '\0';

		//printf("it is %s\n",time);
		//printf("and today is %s\n",date);
		
		//lcdClear(fd);
		lcdPosition(fd, 0 ,0);
		lcdPuts(fd, date);

		lcdPosition(fd, 0 ,1);
		lcdPuts(fd, time);
}

void beep(int fd, int times) {
	int i;
	for (i = 0; i < times; i++) {
		printCurrentTimeToScreen(fd);
		digitalWrite(BeepPin, BeepOn);  //beep on
		delay(100);                  //delay
		digitalWrite(BeepPin, BeepOff); //beep off
		delay(100);                  //delay
	}
	digitalWrite(BeepPin, BeepOff); //beep off
}

void saveToFile(char* path, int hour, int min, int on, int startPc) {
	FILE *file = fopen(path,"w");
	fprintf(file, "%d:%d:%d:%d", hour, min, on, startPc);
	fclose(file);
}

void startPc(int fd) {
	lcdClear(fd);
	lcdPosition(fd, 0 ,0);
	lcdPuts(fd, "starting pc");

	//TODO move to own class

	unsigned char tosend[102];
	unsigned char mac[6];

	/** first 6 bytes of 255 **/
	for(int i = 0; i < 6; i++) {
	tosend[i] = 0xFF;
	}
	/** store mac address **/
	mac[0] = 0x00;
	mac[1] = 0xD8;
	mac[2] = 0x61;
	mac[3] = 0x36;
	mac[4] = 0x98;
	mac[5] = 0xE8;
	/** append it 16 times to packet **/
	for(int i = 1; i <= 16; i++) {
		memcpy(&tosend[i * 6], &mac, 6 * sizeof(unsigned char));
	}

    	int socketSD = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
    	if (socketSD <= 0) {
        	//NSLog(@"Error: Could not open socket.");
        	return;
    	}
    
    	// set socket options enable broadcast
    	int broadcastEnable = 1;
    	int ret = setsockopt(socketSD, SOL_SOCKET, SO_BROADCAST, &broadcastEnable, sizeof(broadcastEnable));
    	if (ret) {
        	//NSLog(@"Error: Could not open set socket to broadcast mode");
        	close(socketSD);
        	return;
    	}
    
    	// Configure the port and ip we want to send to
    	struct sockaddr_in broadcastAddr;
    	memset(&broadcastAddr, 0, sizeof(broadcastAddr));
    	broadcastAddr.sin_family = AF_INET;
    	inet_pton(AF_INET, "255.255.255.255", &broadcastAddr.sin_addr);
    	broadcastAddr.sin_port = htons(9);
    	
    	ret = sendto(socketSD, &tosend, sizeof(unsigned char) * 102, 0, (struct sockaddr*)&broadcastAddr, sizeof(broadcastAddr));
    	if (ret < 0) {
        	//NSLog(@"Error: Could not open send broadcast.");
        	close(socketSD);
        	return;
    	}
    
    	close(socketSD);
	
	sleep(2);
}

int main(char* args)
{	
	int fd;
	int i;
	if (wiringPiSetup() == -1){
		exit(1);
	}

	initButtons();

	fd = lcdInit(2,16,4, 2,3, 6,5,4,1,0,0,0,0); //see /usr/local/include/lcd.h

	if (fd == -1){
		printf("lcdInit 1 failed\n") ;
		return 1;
	}
	alarmTime = malloc(sizeof (struct tm));
	if (!alarmTime) {
		return 1;
	}
	
	alarmOn = 0;
	startPcOn = 0;
	alarmTime->tm_min = 30;
	alarmTime->tm_hour = 7;
	lcdClear(fd);
	lcdPosition(fd, 0 ,0);
	lcdPuts(fd, "trying to read  from file");
	sleep(1);
	//if executed from shell script, this file lives in ~/
	FILE *file = fopen(Path,"r");
	if (file) {
		int min;
		int hour;
		int on;	
		int startPc;		
		int a = fscanf(file, "%d:%d:%d:%d", &hour, &min, &on, &startPc);
		if (min >= 0 && min < 60 && hour >= 0 && hour <= 24) {
			alarmTime->tm_min = min;
			alarmTime->tm_hour = hour;
			alarmOn = on;
			startPcOn = startPc;
			lcdClear(fd);
			lcdPosition(fd, 0 ,0);
			lcdPuts(fd, "successfully    read from file");
		} else {
			lcdClear(fd);
			lcdPosition(fd, 0 ,0);
			lcdPuts(fd, "invalid file");
		}
		fclose(file);
		sleep(2);
	} else {
		lcdClear(fd);
		lcdPosition(fd, 0 ,0);
		lcdPuts(fd, "could not read from file");
	}
	int beeping = 0;
	int firstOne = 0;
	int snoozed = 0;
	char* editingText = malloc(sizeof(char) * 14);
	editingText[5] = '\0';
	while(1) {
		printCurrentTimeToScreen(fd);
		
		if (beeping) {
			if (!isSameMinute(alarmTime)) {
				snoozed = 1;
			}
			if(digitalRead(SetPin) == 0 || digitalRead(SelectPin) == 0 || digitalRead(CyclePin) == 0 || digitalRead(StartPin) == 0) {
				snoozed = 1;
				while(digitalRead(SetPin) == 0 || digitalRead(SelectPin) == 0 || digitalRead(CyclePin) == 0 || digitalRead(StartPin) == 0);
			}
		} else {
			if(digitalRead(StartPin) == 0) {
				lcdClear(fd);
				lcdPosition(fd, 0 ,0);
				char* startingString = malloc(sizeof(char) * 18);
				int time = 3000;
				while(!digitalRead(StartPin) && time > 0) {
					sprintf(startingString, "starting in %d ", time);
					lcdPosition(fd, 0 ,0);
					lcdPuts(fd, startingString);
					time -= 100;
					delay(100);
				}
				free(startingString);
				if(time <= 0) {
					startPc(fd);
				}
				lcdClear(fd);
			}
			// the set button arms/disarmes the alarm
			if (digitalRead(SetPin) == 0) {
				alarmOn = !alarmOn;
				saveToFile(Path, alarmTime->tm_hour, alarmTime->tm_min, alarmOn, startPcOn);
				while(!digitalRead(SetPin));
			}
			int pos = 0;
			int editing = 0;
			if (digitalRead(SelectPin) == 0) {
				editing = 1;
				while(!digitalRead(SelectPin));
				lcdClear(fd);
			}
			while(editing) {
				if (digitalRead(SelectPin) == 0) {
					//if we stop editing the alarmTime, we save it to the save file
					editing = 0;
					saveToFile(Path, alarmTime->tm_hour, alarmTime->tm_min, alarmOn, startPcOn);
					while(!digitalRead(SelectPin));
					lcdClear(fd);
					break;
				}
				if (digitalRead(SetPin) == 0) {
					if (pos == 0) {
						//add one hour
						alarmTime->tm_hour = (alarmTime->tm_hour + 1) % 24;
					} else if (pos == 1) {
						//add one minute
						alarmTime->tm_min = (alarmTime->tm_min + 1) % 60;
					} else if (pos == 2) {
						startPcOn = !startPcOn;
					}
					while(!digitalRead(SetPin));
				}
				if (digitalRead(CyclePin) == 0) {
					pos = (pos + 1) % 3;
					while(!digitalRead(CyclePin));
				}
				
				if(alarmTime->tm_hour < 10) {
					if(alarmTime->tm_min < 10) {
						sprintf(editingText, startPcOn ? "0%d:0%d start:yes" : "0%d:0%d start:no ", alarmTime->tm_hour, alarmTime->tm_min);
					} else {
						sprintf(editingText, startPcOn ? "0%d:%d start:yes" : "0%d:%d start:no ", alarmTime->tm_hour, alarmTime->tm_min);
					}
				} else {
					if(alarmTime->tm_min < 10) {
						sprintf(editingText, startPcOn ? "%d:0%d start:yes" : "%d:0%d start:no ", alarmTime->tm_hour, alarmTime->tm_min);
					} else {
						sprintf(editingText, startPcOn ? "%d:%d start:yes" : "%d:%d start:no ", alarmTime->tm_hour, alarmTime->tm_min);
					}
				}
				lcdPosition(fd, 0 ,0);
				lcdPuts(fd, editingText);
				lcdPosition(fd, 0, 1);
				if(pos == 0) {	
					lcdPuts(fd, "^^           ");
				} else if (pos == 1) {
					lcdPuts(fd, "   ^^        ");
				} else if (pos == 2) {
					lcdPuts(fd, "            ^^^");
				}
				delay(100);
			}
		}

		//logic to handle beeping and snoozing behavior		
		if (!snoozed) {
			if (alarmOn && isSameMinute(alarmTime)) {
				beeping = 1;
				if (startPcOn) {
					firstOne++;
				}
			}
		} else {
			beeping = 0;
			firstOne = 0;
			if (!isSameMinute(alarmTime)) {
				snoozed = 0;
			}
		}
				
		//beep
		if (beeping) {
			if(firstOne == 1) {
				startPc(fd);
			}
			beep(fd, 1);
		}
		
		delay(150);
	}
	return(0);
}