# Alarm Clock
This is the code for a simple Alarm Clock build with a Raspberry Pi.

I used the Sunfounder example to learn how to easily use an LCD-Scrren to display the current date and time. Go ahead and check them out [here](https://github.com/sunfounder/Sunfounder_SuperKit_C_code_for_RaspberryPi)

To control the LCD I used the wiringPi Library which can be found [here](https://github.com/WiringPi/WiringPi)

## Pin Usage for the LCD Screen:

D4---GPIO6

D5---GPIO5

D6---GPIO4

D7---GPIO1

RS---GPIO2

RW---GND

CE---GPIO3

## Pin Usage for the Buttons

TODO BUTTON WIRING

The buttons are always wired against GND.

SelectButton---GPIO29

CycleButton---GPIO28

SetButton---GPIO25

StartButton---GPIO24

## Pin Usage for the Beepthing

Again it is wired against GND, mind + and - of the Beeper.

Beeper---GPIO27


## Code

The code lives in the alarmClock.c Class. To Compile siply use the ````make```` command.

Make yields the executable ````LCD```` file, which then needs to be executed as root e.g. ````sudo ./LCD````

The alarm-clock is cabable of starting a PC via a magic packet. To use this feature, the target mac address must be hard coded in mac\[\] in alarmClock.c
